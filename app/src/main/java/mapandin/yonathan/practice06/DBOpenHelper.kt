package mapandin.yonathan.practice06

import android.content.Context
import android.database.sqlite.SQLiteDatabase
import android.database.sqlite.SQLiteOpenHelper

class DBOpenHelper(context : Context):SQLiteOpenHelper(context,DB_Name,null,DB_Ver) {

    override fun onCreate(db: SQLiteDatabase?) {
        val tMhs = "create table mhs(nim text primary key, nama text not null, id_prodi int not null)"


        val tProdi = "create table prodi(id_prodi integer primary key autoincrement, nama_prodi text not null)"


        val tMatkul = "create table Matakuliah (id_matakuliah integer primary key autoincrement, id_prodi integer primary key autoincrement, matakuliah text not null)"


        val tNilai = "create table Nilai (nama text not null, matakuliah text not null, id_matakuliah integer primary key autoincrement)"


        val insProdi = "insert into prodi(nama_prodi) values('Teknik Informatika'),('Akuntansi'),('Teknik Mesin')"



        db?.execSQL(tMhs)
        db?.execSQL(tNilai)
        db?.execSQL(tMatkul)
        db?.execSQL(tProdi)
        db?.execSQL(insProdi)

    }

    override fun onUpgrade(db: SQLiteDatabase?, oldVersion: Int, newVersion: Int) {

    }

    companion object {
        val DB_Name = "mahasiswa"
        val DB_Ver = 1
    }
}
package mapandin.yonathan.practice06

import android.database.sqlite.SQLiteDatabase
import android.graphics.Color
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.MenuItem
import android.view.View
import androidx.fragment.app.FragmentTransaction
import com.google.android.material.bottomnavigation.BottomNavigationView
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity(), BottomNavigationView.OnNavigationItemSelectedListener {

    lateinit var fragProdi : FragmentProdi
    lateinit var fragMhs : FragmentMahasiswa
    lateinit var ft : FragmentTransaction
    lateinit var fragBagi : FragmentPembagian
    lateinit var fragPer : FragmentPerkalian
    lateinit var db : SQLiteDatabase


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        bottomNavigationView2.setOnNavigationItemSelectedListener(this)
        fragProdi = FragmentProdi()
        fragMhs = FragmentMahasiswa()
        fragBagi = FragmentPembagian()
        fragPer = FragmentPerkalian()
    }

    fun getDbObject() : SQLiteDatabase {
        return db
    }

    override fun onNavigationItemSelected(item: MenuItem): Boolean {
        when(item.itemId){
            R.id.itemProdi ->{
                ft = supportFragmentManager.beginTransaction()
                ft.replace(R.id.frameLayout,fragProdi).commit()
                FrameLayout.setBackgroundColor(Color.argb(245,255,255,255))
                FrameLayout.visibility = View.VISIBLE
            }
            R.id.itemMhs ->{
                ft = supportFragmentManager.beginTransaction()
                ft.replace(R.id.frameLayout,fragMhs).commit()
                FrameLayout.setBackgroundColor(Color.argb(245,225,255,255))
                FrameLayout.visibility = View.VISIBLE
            }
            R.id.itemBagi -> {
                ft = supportFragmentManager.beginTransaction()
                ft.replace(R.id.frameLayout,fragBagi).commit()
                frameLayout.setBackgroundColor(Color.argb(245,255,225,255))
                frameLayout.visibility = View.VISIBLE
            }
            R.id.fragPer -> {
                ft = supportFragmentManager.beginTransaction()
                ft.replace(R.id.frameLayout,fragPer) .commit()
                frameLayout.setBackgroundColor(Color.argb(245,255,255,225))
                frameLayout.visibility = View.VISIBLE
            }
            R.id.itemAbout -> FrameLayout.visibility = View.GONE
        }
        return true
    }
}
